module G = Graph.Pack.Graph

type t = G.t
type vertex = G.V.t
type path = G.V.t list

let vertices (g : t) =
  G.fold_vertex (fun v l -> v :: l) g []

let path_to_string (p : path) =
  List.map (fun x -> string_of_int @@ G.V.label x) p
  |> String.concat ","

(* more modules available, e.g. graph traversal with depth-first-search *)
(* module D = Graph.Traverse.Dfs(G) *)

let compare_float (x : float) (y : float) =
    if x < y then -1 else if x > y then 1 else 0

module W =
struct
  type t = float
  let zero = 0.0
  let add = (+.)
  let compare = compare_float
  type edge = G.edge
  let weight _ = 1.0
end

module P = Graph.Path.Dijkstra(G)(W)

(* Graph.Graphviz.DotAttributes. *)
(* module for creating dot-files *)
module Dottable =
struct
  include G (* use the graph module from above *)
  let edge_attributes edge =
    let lab = string_of_int @@ G.E.label edge in
    [`Label lab ; `Color 4711]
  let default_edge_attributes _ = []
  let get_subgraph _ = None
  let vertex_attributes i = [`Shape `Box ; `Label (string_of_int @@ G.V.label i)]
  let vertex_name v = string_of_int (G.V.label v)
  let default_vertex_attributes _ = []
  let graph_attributes _ = []
end


module Dot = Graph.Graphviz.Dot(Dottable)

let itv x y =
  let rec loop i =
    if i = y then
      [y]
    else
      i :: (loop (i+1))
  in
  loop x

let random_graph nverts edgep =
  let verts = itv 0 (nverts - 1) in
  let verts = List.map G.V.create verts in
  let all_edges =
    ListLabels.fold_left ~init:[] ~f:(fun acc v1 ->
        ListLabels.fold_left ~init:acc ~f:(fun acc v2 ->
            if G.V.label v1 >= G.V.label v2 then
              acc
            else if (Random.float 1.0 < edgep) then
              (v1, v2) :: acc
            else
              acc
          ) verts
      ) verts in
  let graph = G.create () in
  ListLabels.iter all_edges ~f:(fun (v1, v2) ->
      G.add_edge graph v1 v2
    ) ;
  graph

let to_met : t -> (module Sigs.OrderedFinMet with type t = G.V.t) =
  fun graph ->
    (module struct
      include G.V
      let d x y =
        snd (P.shortest_path graph x y)
      let points =
        Array.of_list @@ vertices graph
    end)

let neighbours (g : t) (v : G.V.t) =
  try G.succ g v with
  | e ->
    let s = Printexc.to_string e in
    Printf.eprintf "exception: %s\n" s ;
    exit 0

let pick_vertex_uniformly g =
  let nverts = G.nb_vertex g in
  Random.int nverts

let squash_if_equal (l : ('a * float) list) =
  let rec loop l current_repr_and_class class_acc =
    match l with
    | [] ->
      (match current_repr_and_class with
       | None -> List.rev class_acc
       | Some (current_repr, current_class) ->
         List.rev ((current_repr, current_class) :: class_acc))
    | (x, d) :: tl ->
      (match current_repr_and_class with
       | None ->
         let current_repr_and_class = Some (d, [x]) in
         loop tl current_repr_and_class class_acc
       | Some (current_repr, current_class) ->
         if current_repr = d then
           let current_repr_and_class = Some (d, x :: current_class) in
           loop tl current_repr_and_class class_acc
         else
           loop tl (Some(d, [x])) ((current_repr, current_class) :: class_acc))
  in
  loop l None []

type ranking = (float * vertex list) list

let rank g x =
  let module M = (val (to_met g)) in
  let vs     = vertices g in
  let dists  = List.map (fun v -> (v, M.d x v)) vs in
  let sorted = List.sort (fun (_, d) (_, d') -> compare_float d d') dists in
  squash_if_equal sorted
