open Sigs

module Stats = StaTz.Stats
open Stats

let remove_cycles (type t) (module X : Ordered with type t = t) (path : t list) : t list =
  let rec remove_back_to elt loop_free_acc =
    match loop_free_acc with
    | [] -> []
    | x :: rev_prefix ->
      if X.compare x elt = 0 then
        loop_free_acc
      else
        remove_back_to elt rev_prefix
  in
  let rec loop path loop_free_acc =
    match path with
    | [] -> List.rev loop_free_acc
    | x :: rest ->
      if List.exists (fun y -> X.compare x y = 0) loop_free_acc then
        loop rest (remove_back_to x loop_free_acc)
      else
        loop rest (x :: loop_free_acc)
  in
  loop path []

let remove_duplicates (type t) (module X : Ordered with type t = t) (path : t list) : t list =
  let rec loop path prev acc =
    match path, prev with
    | [], _ ->
      List.rev acc
    | x :: rest, None ->
      loop rest (Some x) (x :: acc)
    | x :: rest, Some y ->
      if X.compare x y = 0 then
        loop rest (Some x) acc
      else
        loop rest (Some x) (x :: acc)
  in
  loop path None []

let walk_until
    (type t)
    (start : t)
    (walk : t -> t Stats.gen)
    (predicate : t -> bool) : (t list) Stats.gen =
  let rec loop current acc =
    if predicate current then
      (* let _ = Printf.printf "walk stopped\n" in *)
      List.rev acc
    else
      let next = Stats.sample_gen (walk current) in
      loop next (next :: acc) in
  Stats.generative ~sampler:begin fun () ->
    (* let _ = Printf.printf "walk start\n" in *)
    loop start [start]
    end

let walk_length_until
    (type t)
    (start : t)
    (walk : t -> t Stats.gen)
    (predicate : t -> bool) : int Stats.gen =
  let rec loop current acc =
    if predicate current then
      acc
    else
      let next = Stats.sample_gen (walk current) in
      loop next (acc + 1) in
  Stats.generative ~sampler:begin
      fun () -> loop start 1
    end

let path
    (type t)
    (module X : Ordered with type t = t)
    (walk : t -> t gen)
    (start : t)
    (stop : t) : (t list) gen =
  walk_until start walk (fun x -> X.compare stop x = 0)

let cycle_free_path
    (type t)
    (module X : Ordered with type t = t)
    (walk : t -> t gen)
    (start : t)
    (stop : t) : (t list) gen =
  let path = walk_until start walk (fun x -> X.compare stop x = 0) in
  Stats.map_gen (fun path ->
      remove_cycles (module X) path) path

let cycle_free_path_length
    (type t)
    (module X : Ordered with type t = t)
    (walk : t -> t gen)
    (start : t)
    (stop : t) : int gen =
  let path = walk_until start walk (fun x -> X.compare stop x = 0) in
  Stats.map_gen (fun path ->
      List.length (remove_cycles (module X) path) - 1) path

let path_length
    (type t)
    (module X : Ordered with type t = t)
    (walk : t -> t gen)
    (start : t)
    (stop : t) : int gen =
  let path = walk_until start walk (fun x -> X.compare stop x = 0) in
  Stats.map_gen (fun path ->
      List.length (remove_duplicates (module X) path) - 1) path

let average_cycle_free_path_length
    (type t)
    (module X : OrderedFinMet with type t = t)
    (walk : t -> t gen)
    (nsamples : int) : t Metric.fin =
  (module struct
    include X
    let d (start : t) (stop : t) =
      cycle_free_path_length (module X) walk start stop
      |> Stats.map_gen float_of_int
      |> Stats.empirical_of_generative ~nsamples
      |> Stats.mean (module StaTz.Structures.Float)
  end)

let average_path_length
    (type t)
    (module X : OrderedFinMet with type t = t)
    (walk : t -> t gen)
    (nsamples : int) : t Metric.fin =
  (module struct
    include X
    let d (start : t) (stop : t) =
      path_length (module X) walk start stop
      |> Stats.map_gen float_of_int
      |> Stats.empirical_of_generative ~nsamples
      |> Stats.mean (module StaTz.Structures.Float)
  end)



(* TODO:
   - check symmetry of estimate
   - check violation of triangular inequality
   - check distortion wrt original space
*)
