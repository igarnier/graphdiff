type 'a t = (float * 'a list) list

let show_full pp_elt (ranking : 'a t) =
  let pp_list l = List.map pp_elt l |> String.concat "," in
  List.map (fun (d, l) ->
      Printf.sprintf "[ %.1f : { %s } ]"
        d
        (pp_list l)) ranking
  |> String.concat " ; "

let show pp_elt (ranking : 'a t) =
  let pp_list l = List.map pp_elt l |> String.concat "," in
  List.map (fun (_d, l) ->
      Printf.sprintf "{ %s }" (pp_list l)) ranking
  |> String.concat " ; "

let squash_if_equal (l : ('a * float) list) =
  let rec loop l current_repr_and_class class_acc =
    match l with
    | [] ->
      (match current_repr_and_class with
       | None -> List.rev class_acc
       | Some (current_repr, current_class) ->
         List.rev ((current_repr, current_class) :: class_acc))
    | (x, d) :: tl ->
      (match current_repr_and_class with
       | None ->
         let current_repr_and_class = Some (d, [x]) in
         loop tl current_repr_and_class class_acc
       | Some (current_repr, current_class) ->
         if current_repr = d then
           let current_repr_and_class = Some (d, x :: current_class) in
           loop tl current_repr_and_class class_acc
         else
           loop tl (Some(d, [x])) ((current_repr, current_class) :: class_acc))
  in
  loop l None []

let ranking_of_point (type elt) ((module M) : elt Metric.fin) (x : elt) =
  let dists  = Array.map (fun v -> (v, M.d x v)) M.points in
  Array.sort (fun (_, d) (_, d') -> Base.Float.compare d d') dists ;
  squash_if_equal (Array.to_list dists)

(* Bad: should parameterise with equality function. *)
let rank_of elt (ranking : 'a t) =
  let rec loop i l =
    match l with
    | [] -> None
    | (_d, pts) :: tl ->
      if List.mem elt pts then
        Some i
      else
        loop (i + 1) tl
  in loop 0 ranking

(* assumes the rankings are supported by the same sets of points *)
let rank_dist (r1 : 'a t) (r2 : 'a t) =
  let res, _ =
    List.fold_left (fun (acc, index1) (_, pts) ->
        let acc =
          List.fold_left (fun acc pt ->
              match rank_of pt r2 with
              | None   -> assert false
              | Some index2 ->
                acc + abs (index1 - index2)
            ) acc pts in
        (acc, index1 + 1)
      ) (0, 0) r1 in
  res

let of_space (type elt) ((module M) : elt Metric.fin) : elt t Metric.fin =
  (module struct
    type nonrec t = elt t
    let d r1 r2 = float @@ rank_dist r1 r2
    let points = Array.map (ranking_of_point (module M)) M.points
  end)
