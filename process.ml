type 'a t =
  | Stop
  | Continue of (unit -> 'a * 'a t)

type 'a path = 'a list

let execute_until (proc : 'a t) (pred : 'a -> bool) : 'a path =
  let rec loop proc acc =
    match proc with
    | Stop -> List.rev acc
    | Continue f ->
      let out, next = f () in
      if pred out then
        loop Stop (out :: acc)
      else
        loop next (out :: acc)
  in
  loop proc []

let from_walk (init : 'a) (walk : 'a -> 'a Stats.gen) =
  let rec loop init =
    let dist = walk init in
    let spl  = Stats.sample_gen dist in
    Continue (fun () -> (spl, loop spl))
  in loop init
