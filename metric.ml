open Numerics

type 'a t   = (module Sigs.Met with type t = 'a)
type 'a ord = (module Sigs.OrderedMet with type t = 'a)
type 'a fin = (module Sigs.FinMet with type t = 'a)
type 'a ordfin = (module Sigs.OrderedFinMet with type t = 'a)

(* Restrict a metric space to a finite subset. *)
let restrict
    (type elt)
    (module M : Sigs.Met with type t = elt)
    (points : elt array) : elt fin =
  (module struct
    include M
    let points = points
  end)

(* Normalize a metric so that the distance between any two points is
   bounded by one. *)
let normalize (type elt) (module M : Sigs.FinMet with type t = elt) : elt fin =
  let card = Array.length M.points in
  let maxd = ref 0.0 in
  for i = 0 to card - 1 do
    for j = i+1 to card - 1 do
      maxd := (max !maxd (M.d M.points.(i) M.points.(j)))
    done
  done ;
  let imaxd = 1.0 /. !maxd in
  (module struct
    include M
    let d x y =
      (M.d x y) *. imaxd
  end)

(* Compute the pullback of a metric along a map. *)
let pullback
    (type elt1 elt2)
    (f : elt1 -> elt2)
    (module M : Sigs.Met with type t = elt2) : elt1 t =
  (module struct
    type t = elt1
    let d x y = M.d (f x) (f y)
  end)

(* Compute the pullback of a metric along a map. *)
let pullback_fin
    (type elt1 elt2)
    (points : elt1 array)
    (f : elt1 -> elt2)
    (module M : Sigs.Met with type t = elt2) : elt1 fin =
  (module struct
    type t = elt1
    let points = points
    let d x y = M.d (f x) (f y)
  end)

(* Compute the (category-theoretical) product of two metric spaces. *)
let sup_product (type x y) ((module M1) : x t) ((module M2) : y t) : (x * y) t =
  (module struct
    type t = M1.t * M2.t

    let d (x1, y1) (x2, y2) =
      max (M1.d x1 x2) (M2.d y1 y2)
  end)

let lp_product (type x y) ((module M1) : x t) ((module M2) : y t) (p  : float) : (x * y) t =
  (module struct
    type t = M1.t * M2.t

    let d (x1, y1) (x2, y2) =
      let d1 = M1.d x1 x2 in
      let d2 = M2.d y1 y2 in
      (d1 ** p +. d2 ** p) ** (1.0 /. p)
  end)

let array_max (arr : float array) =
  let res = ref neg_infinity in
  for i = 0 to Array.length arr - 1 do
    if arr.(i) > !res then
      res := arr.(i)
  done ;
  !res

let sup_power (type x) ((module M) : x t) : x array t =
  (module struct
     type t = M.t array

    let d array1 array2 =
      if Array.length array1 <> Array.length array2 then
        failwith "sup_power: array of different lengths"
      else
        Array.map2 M.d array1 array2
        |> array_max
  end)

let lp_power (type x) ((module M) : x t) (p  : float) : x array t =
  (module struct
    type t = M.t array

    let d array1 array2 =
      if Array.length array1 <> Array.length array2 then
        failwith "sup_power: array of different lengths"
      else
        let res =
          Array.map2 M.d array1 array2
          |> Array.fold_left (fun acc x -> acc +. x ** p) 0.0 in
        res ** (1.0 /. p)
  end)

let rec cartesian_product l1 l2 acc =
  match l1 with
  | [] -> acc
  | hd :: tl ->
     cartesian_product tl l2 ((List.map (fun y -> (hd, y)) l2) @ acc)

let cartesian_product l1 l2 = cartesian_product l1 l2 []
let array_cartesian a1 a2 =
  Array.of_list (cartesian_product (Array.to_list a1) (Array.to_list a2))

let to_matrix (type elt) ((module M) : elt fin) =
  array_cartesian M.points M.points
  |> Array.map (fun (x, y) -> M.d x y)

(* The distortion between two metrics is the sup norm of their difference.
   We make no assumption on the symmetry of the metrics, just in case
   we have to handle quasimetrics ... *)
let distortion (type elt) ((module X) : elt fin) ((module E) : elt fin) =
  if X.points <> E.points then
    failwith "distortion: spaces have distinct underlying sets" ;
  let prod = array_cartesian X.points X.points in
  let delta = Array.map (fun (x, y) ->
      abs_float (X.d x y -. E.d x y)) prod in
  array_max delta

module Stats = StaTz.Stats

let kantorovich (type elt) ((module X) : elt t) : elt Stats.fin_prb t =
  (module struct
    type t = elt Stats.fin_prb

    let d prob1 prob2 =
      if Stats.compare_prb prob1 prob2 = 0 then
        0.0
      else
        let `Probability prb1 = Stats.raw_data_probability prob1 in
        let `Probability prb2 = Stats.raw_data_probability prob2 in
        let elts1 = Array.of_list (List.map fst prb1) in
        let elts2 = Array.of_list (List.map fst prb2) in
        let probas1 = Array.of_list (List.map snd prb1) in
        let probas2 = Array.of_list (List.map snd prb2) in
        let metric =
          let n1 = Array.length probas1 in
          let n2 = Array.length probas2 in
          Float64.Mat.init ~lines:n1 ~cols:n2 ~f:(fun i j ->
              X.d elts1.(i) elts2.(j)) in
        let v1 = Bigarray.Array1.of_array Bigarray.Float64 Bigarray.c_layout probas1 in
        let v2 = Bigarray.Array1.of_array Bigarray.Float64 Bigarray.c_layout probas2 in
        let rec loop num_iter =
          let res = Camlot.kantorovich ~x:v1 ~y:v2 ~d:metric ~num_iter in
          match res with
          | Camlot.Infeasible ->
             failwith "kantorovich: infeasible"
          | Camlot.Unbounded ->
             failwith "kantorovich: unbounded"
          | Camlot.Optimal { cost ; _ } ->
             cost
          | Camlot.MaxIterReached _ ->
             loop (num_iter * 2) in
        loop 4000

  end)

(* Pretty-printing - stolen from Owl *)
module Printing =
struct
  let _calc_col_width table col_idx =
    let col_width = ref 0 in
    for row_idx = 0 to Array.length table - 1 do
      let w = String.length table.(row_idx).(col_idx) in
      col_width := max !col_width w
    done;
    !col_width


  let _align_str s n =
    let w = max 0 (n - String.length s) in
    (String.make w ' ') ^ s


  let _chunk_table max_row max_col row_num col_num =
    let _discontinuous max_w w =
      let half = max_w / 2 in
      let fst_half = Array.init half (fun i -> i) in
      let snd_half = Array.init half (fun i -> i + w - half) in
      Array.(append (append fst_half [|-1|]) snd_half)
    in
    let row_indices =
      match row_num <= max_row with
      | true  -> Array.init row_num (fun i -> i)
      | false -> _discontinuous max_row row_num
    in
    let col_indices =
      match col_num <= max_col with
      | true  -> Array.init col_num (fun i -> i)
      | false -> _discontinuous max_col col_num
    in
    row_indices, col_indices


  let _make_header ?(row_prefix="R") ?(col_prefix="C") elements pp =
    let row_header = Array.map (fun x ->
        Printf.sprintf "%s%s" row_prefix (pp x)
      ) elements
    in
    let col_header = Array.map (fun x ->
        Printf.sprintf "%s%s" col_prefix (pp x)
      ) elements
    in
    row_header, col_header


  let _fill_table d elements =
    let n = Array.length elements in
    let table = Array.make_matrix n n "" in

    Array.iteri (fun i x ->
        Array.iteri (fun j y ->
            let e = d x y in
            let s = string_of_float e in
            table.(i).(j) <- s
          ) elements
      ) elements;

    table

  let _glue_headers row_header col_header table =
    let table = Array.append [|col_header|] table in
    let row_header = Array.append [|""|] row_header in
    Array.mapi (fun i row ->
        Array.append [|row_header.(i)|] row
      ) table


  let _format_table hline table =
    let row_num = Array.length table in
    let col_num = Array.length table.(0) in
    let col_width = Array.init col_num (fun i -> _calc_col_width table i) in
    let out_s = ref "\n" in

    for i = 0 to row_num - 1 do
      (* print hline if necessary *)
      if hline = true && i < 2 then (
        let hline = Array.init col_num (fun j ->
            if j = 0 then String.make col_width.(j) ' '
            else "+" ^ String.make col_width.(j) '-'
          )
        in
        let s = Array.fold_left (fun acc a -> acc ^ a) "" hline in
        out_s := !out_s ^ s ^ "\n";
      );
      (* print rest of content *)
      for j = 0 to col_num - 1 do
        let new_s = _align_str table.(i).(j) col_width.(j) in
        out_s := !out_s ^ new_s ^ " "
      done;
      out_s := !out_s ^ "\n"
    done;

    !out_s

  let metric_to_string ?(header=true) elt_to_str_fun d elements =
    (* common case: rank > 0 *)
    let tbody = _fill_table d elements in
    let row_header, col_header = _make_header elements elt_to_str_fun in
    let table = match header with
      | true  -> _glue_headers row_header col_header tbody
      | false -> tbody
    in
    _format_table false table

  let print_raw ?header elt_to_str_fun formatter d elements =
    let s = metric_to_string ?header elt_to_str_fun d elements in
    Format.open_box 0;
    Format.fprintf formatter "%s" s;
    Format.close_box ()
end

let print ?header (type elt) ((module M) : elt fin) (pp : elt -> string) formatter =
  Printing.print_raw ?header pp formatter M.d M.points
