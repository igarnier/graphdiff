open Sgraph

module Stats = StaTz.Stats

let graph = random_graph 10 0.3

let () =
  if Array.length Sys.argv > 1 then
    (Printf.eprintf "Outputting to %s\n" Sys.argv.(1) ;
     flush stderr ;
     let fd = open_out Sys.argv.(1) in
     Dot.output_graph fd graph ;
     close_out fd)
  else
    Dot.output_graph stdout graph

let walk = Walks.lazy_random_walk 0.1 graph

let vertices = Sgraph.vertices graph

let _ =
  Printf.printf "%d vertices\n" (List.length vertices)

let truth = Sgraph.to_met graph

let truth_normalized =
  Metric.normalize (module struct include (val truth) end)

let _ =
  Metric.print truth_normalized
    (fun x -> string_of_int (Sgraph.G.V.label x))
    Format.std_formatter

(* distribution over lengths of paths between fixed endpoints *)
let dist_path (type t) (module X : Sigs.Ordered with type t = t) (start : t) (stop : t) walk =
  Estimator.walk_until start walk (fun x -> X.compare stop x = 0)
  |> Stats.map_gen (fun x -> float @@ List.length x)
  |> Stats.empirical_of_generative ~nsamples:1000
  |> Stats.fin_prb_of_empirical (module StaTz.Structures.Float)

(* Vector of distributions over paths starting from a given point *)
let hitting_time_dist (type t) (module X : Sigs.OrderedFinMet with type t = t) walk =
  let module M = Map.Make(X) in
  let memo_map = ref M.empty in
  fun (start : t) ->
    if M.mem start !memo_map then
      M.find start !memo_map
    else
      let res =
        Array.map (fun stop ->
            dist_path (module X) start stop walk
          ) X.points in
      memo_map := M.add start res !memo_map ;
      res

(* Testing kanto *)

(* let k_n = Metric.kantorovich (module struct type t = float let d x y = abs_float (y -. x) end)
 *
 * let _ =
 *   let module K = (val k_n) in
 *   Printf.printf "dist: %f %f: %f\n" 0.0 10.0 (K.d (Stats.dirac_den 0.0) (Stats.dirac_den 10.0)) *)

(* Feature space *)

(* The monoidal structure seems to work better than the cartesian one (sup_power) *)
let feature_space =
  Metric.lp_power (Metric.kantorovich (module struct type t = float let d x y = abs_float (y -. x) end)) 1.0

(* Inferred space *)
let pullback_space =
  Metric.pullback (hitting_time_dist truth walk) feature_space

let pullback_space_fin =
  Metric.restrict pullback_space (Array.of_list vertices)

let pullback_space_fin_normalized =
  Metric.normalize pullback_space_fin

let _ =
  Metric.print pullback_space_fin_normalized
    (fun x -> string_of_int (Sgraph.G.V.label x))
    Format.std_formatter

(* let distribution =
 *   Printf.printf "Distribution of hitting times\n%!" ;
 *   let dist =
 *     Estimator.path_length
 *       (module struct include (val truth) end)
 *       walk
 *       (List.nth vertices 6)
 *       (List.nth vertices 9)
 *     |> Stats.map_gen float
 *     |> Stats.empirical 1000 in
 *   Printf.printf "Plotting\n%!" ;
 *   Stats.plot dist
 *
 *
 * let estimation_with_cycle_free_path_length () =
 *   Printf.printf "Estimating with average cycle free path length\n%!" ;
 *   let cycle_free_estimate =
 *     Estimator.average_cycle_free_path_length
 *       truth
 *       walk
 *       1000 in
 *   Printf.printf "Computing distortion... %!" ;
 *   let dist = Metric.distortion (module struct include (val truth) end) cycle_free_estimate in
 *   Printf.printf "Distortion loop-free vs true: %f\n%!" dist ;
 *   Printf.printf "Ranking of estimate:\n%!" ;
 *   let pr_rank = Ranking.show (fun x -> string_of_int @@ G.V.label x) in
 *   let (module R) = Ranking.of_space cycle_free_estimate in
 *   Array.iter (fun x -> Printf.printf "%s\n" @@ pr_rank x) R.points ;
 *   let (module R) = Ranking.of_space (module struct include (val truth) end) in
 *   Array.iter (fun x -> Printf.printf "%s\n" @@ pr_rank x) R.points
 *
 * let _ =
 *   print_newline ()
 *
 * let estimation_with_path_length () =
 *   Printf.printf "Estimating with average path length\n%!" ;
 *   let estimate =
 *     Estimator.average_path_length
 *       truth
 *       walk
 *       1000 in
 *   Printf.printf "Computing distortion... %!" ;
 *   let dist = Metric.distortion (module struct include (val truth) end) estimate in
 *   Printf.printf "Distortion loop-free vs true: %f\n%!" dist ;
 *   Printf.printf "Ranking of estimate:\n%!" ;
 *   let pr_rank = Ranking.show (fun x -> string_of_int @@ G.V.label x) in
 *   let (module R) = Ranking.of_space estimate in
 *   Array.iter (fun x -> Printf.printf "%s\n" @@ pr_rank x) R.points ;
 *   let (module R) = Ranking.of_space (module struct include (val truth) end) in
 *   Array.iter (fun x -> Printf.printf "%s\n" @@ pr_rank x) R.points *)

(* let est =
 *   Estimator.average_path_length
 *     space
 *     walk
 *     1000
 *
 * module Actual_metric = (val (Sgraph.to_met graph))
 *
 * let _ =
 *   let dist = Metric.distortion (module Actual_metric) est in
 *   Printf.printf "distortion loopful vs true: %f\n" dist ;
 *   let dist = Metric.distortion (module Actual_metric) cycle_free_est in
 *   Printf.printf "distortion loop-free vs true: %f\n" dist ;
 *   let elt_to_str v =
 *     string_of_int (Sgraph.G.V.label v) in
 *   Metric.print (module Actual_metric) elt_to_str Format.std_formatter ;
 *   Metric.print cycle_free_est elt_to_str Format.std_formatter ;
 *   Metric.print est elt_to_str Format.std_formatter ;
 *
 *   (\* TODO:
 *      - symmetric random walk
 *      - uniform random walk
 *      - compute actual distance matrix
 *      - plot statistics on /average/ path length
 *        from pair of points
 *      - what is the random walk for which the average path
 *        length matches the metric?
 *        It has to be non-uniform and favor shortest paths ...
 *        If the shortest length is small compared to the
 *        number of actual paths from a point to another, the
 *        average length will always be above the actual metric.
 *
 *      Conjecture: average length overapproximates the metric. *\) *)
