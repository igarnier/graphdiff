open Sigs
open StaTz

let rec rejection_loop dist proba eps x =
  let point = Stats.sample_gen proba in
  if dist point x < eps then
    x
  else
    rejection_loop dist proba eps x

(* generic walk on a measured metric space *)
let epsilon_walk (type t) (module M : Met with type t = t) ~(m : t Stats.gen) ~eps =
  fun x ->
  Stats.generative ~sampler:begin fun () -> rejection_loop M.d m eps x end

(* lazy walk on a graph  *)
let lazy_random_walk
      (lazy_eps : float)
      (g : Sgraph.t) =
  fun (x : Sgraph.vertex) ->
  Stats.generative ~sampler:begin fun () ->
    let n = Sgraph.neighbours g x in
    (* let ns = List.map (fun x ->
     *     string_of_int @@ Sgraph.G.V.label x
     *   ) n in
     * Printf.printf "Random walk at %s, neighbours : %s\n"
     *   (string_of_int @@ Sgraph.G.V.label x) (String.concat "," ns) ; *)
    if Random.float 1.0 > lazy_eps then
      x
    else
      let i = Random.int (List.length n) in
      List.nth n i
    end
